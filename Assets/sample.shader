﻿Shader "Unlit/sample"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color("MainColor", Color) = (1,0,1,1)

	}

	SubShader
	{
		Pass
		{
			CGPROGRAM

			//set fct as vert and frag shader
			#pragma vertex vert
			#pragma fragment frag

			struct in_data
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD;
			};

			struct out_data
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			out_data vert(in_data v)
			{
				out_data o;
				// transform position to clip space
				// (multiply with model*view*projection matrix)
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				// just pass the texture coordinate
				o.uv = v.uv;
				return o;
			}

			sampler2D _MainTex;
			fixed4 _Color;

			fixed4 frag(out_data i) : SV_Target
			{
				//fixed4 col = tex2D(_MainTex, i.uv);
				//fixed4 col = fixed4(1,0,0,0.5);
				fixed4 col = _Color;
				return col;
			}

			ENDCG
		}
	}
}
