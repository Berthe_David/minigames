﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace Billard
{
    public class GameInfo : MonoBehaviour
    {
        Text scoreText;
        Text missText;

        public GameObject[] boulesCouleurs;
        public GameObject bouleBlanche;
        public GameObject gameover;

        public GameObject spawnBoule;
        public GameObject spawnBoules;

        public int total; // Nombre de boulesCouleurs sur le billard
        public int fall; // Nombre tombées dans les trous
        public int miss = 0; // Nombre de fois que la blanche est tombée
        float initY;
        public List<Vector3> offsetPos;
        public List<Quaternion> offsetRota;

        void Awake()
        {
            scoreText = GameObject.Find("Canvas/Score").GetComponent<Text>();
            missText = GameObject.Find("Canvas/Miss").GetComponent<Text>();
            bouleBlanche = GameObject.Find("BouleBlanche");
            boulesCouleurs = GameObject.FindGameObjectsWithTag("BouleCouleur");
            total = boulesCouleurs.Length;
            initY = boulesCouleurs[0].transform.position.y;
            gameover = GameObject.Find("Canvas/GameOver");
        }

        void Start()
        {
            foreach(GameObject go in boulesCouleurs)
            {
                offsetPos.Add(go.transform.position - spawnBoules.transform.position);
                offsetRota.Add(go.transform.rotation);
            }
            gameover.SetActive(false);

            StartCoroutine(Check());
        }

        IEnumerator Check()
        {
            for (;;)
            {
                int i = 0;
                foreach (GameObject go in boulesCouleurs)
                {
                    if (go.transform.position.y <= initY - 0.1)
                    {
                        go.GetComponent<Renderer>().enabled = false;
                        i++;
                    }
                }
                fall = i;
                
                //ça pose problème: erruer etrange
                //GameObject.Find("Canvas/GameOver").SetActive(true); 
                scoreText.text = i + " / " + total;
                
                if (fall == total)
                {
                    gameover.SetActive(true);
                    bouleBlanche.GetComponent<Boule>().enabled = false;
                }

                if(bouleBlanche.transform.position.y <= initY - 0.1)
                {
                    bouleBlanche.GetComponent<Boule>().ResetPosition();
                    miss++;
                    missText.text = "Miss : " + miss;
                }

                yield return new WaitForSeconds(1.0f);
            }
        }

        public void ResetGame()
        {
            var elt = GameObject.Find("Canvas/GameOver");
            if(elt != null)
                elt.SetActive(false);
            bouleBlanche.GetComponent<Boule>().enabled = true;
            bouleBlanche.GetComponent<Boule>().ResetCoups();

            int i = 0;
            miss = 0;
            foreach (GameObject go in boulesCouleurs)
            {
                go.GetComponent<Renderer>().enabled = true;
                go.transform.position = spawnBoules.transform.position + offsetPos[i];
                go.transform.rotation = offsetRota[i];
                go.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
                go.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);

                i++;
            }
            bouleBlanche.transform.position = spawnBoule.transform.position;
            bouleBlanche.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}