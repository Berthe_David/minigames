﻿using UnityEngine;
using System.Collections;

public class Rebound : MonoBehaviour {

    public Vector3 v, n, r;
    public float angle, speed;

    void Start () {
        v = n = r = Vector3.zero;
    }

    void FixedUpdate()
    {
        if (GetComponent<Rigidbody>().velocity.magnitude <= 1)
            GetComponent<Rigidbody>().velocity = Vector3.Lerp(GetComponent<Rigidbody>().velocity,
                                                                new Vector3(0, 0, 0), Time.fixedDeltaTime * 3.0f);
    }

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Entre");
        if (collision.gameObject.tag == "Bord")
        {

            v = collision.relativeVelocity;
            speed = collision.relativeVelocity.magnitude;
            v.y = 0;
            n = collision.contacts[0].normal;
            n.y = 0;
            angle = Vector3.Angle(n.normalized, v.normalized);


            r = -v.normalized + 2 * (n.normalized * Mathf.Cos(angle * Mathf.PI / 180));
            GetComponent<Rigidbody>().velocity = r.normalized * speed;


            //touch = true;
        }
        //else
            //touch = false;
    }
}
