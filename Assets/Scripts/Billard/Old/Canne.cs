﻿using UnityEngine;
using System.Collections;

namespace Billard
{
    public class Canne : MonoBehaviour
    {

        public Camera camera;
        private GameObject canne;
        private GameObject boule;

        private Vector3 initPos;
        public float distToCanne;
        public Vector3 offsetInit, offset;
        public float offsetX;
        public float offsetY;


        //test
        public float dist
        {
            get { return offset.magnitude; }
        }

        public float force
        {
            get { return dist / 2; }
        }


        void Start()
        {
            boule = GameObject.Find("Boule");
            offsetX = offsetY = 0;
            offsetInit = boule.transform.position - Vector3.forward * 3.0f;
            transform.position = offsetInit;
        }


        void FixedUpdate()
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = camera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 100))
                {
                    if (hit.collider.gameObject.name == "Canne")
                    {
                        canne = hit.collider.gameObject;
                        distToCanne = hit.distance;
                        initPos = canne.transform.position;


                    }
                    else
                        canne = null;
                }
            }

            if (Input.GetMouseButton(0))
            {
                if (canne != null)
                {
                    Vector3 vv = camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, distToCanne));
                    vv.y = initPos.y;

                    offset = initPos - vv;
                    //canne.transform.position = vv;
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                if (canne != null)
                {
                    GetComponent<Rigidbody>().isKinematic = false;
                    GetComponent<Rigidbody>().AddForce(transform.forward * offset.magnitude * 1000);
                }
            }
        }

        void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.name == "Boule")
            {
                GetComponent<Rigidbody>().isKinematic = true;
            }
        }
    }
}