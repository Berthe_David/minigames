﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

namespace Billard
{
    public class Boule : MonoBehaviour
    {
        public Camera camera;

        public GameObject spawn;
        GameObject bouleBlanche; // use for raycast
        public GameObject Canvas;

        public Image gauge;
        public Image gaugeGreen;
        public Text coupTxt;

        const float MAX_POWER = 4.0f;

        // Use this for initialization when touched
        public Vector3 initPos;
        Vector3 initPosScreen;

        Vector3 dirShoot;

        float angle;
        float norm; 
        float distToBoule;


        int nbCoups = 0;

        bool mouseDown = false;
        bool mouseUp = false;
        bool mousePress = false;

        void Start()
        {
            Physics.gravity = new Vector3(0, -9.81f, 0);
            

            coupTxt.text = "Coups : " + nbCoups;
            gaugeGreen.fillAmount = 0f;

            gauge.enabled = false;
            gaugeGreen.enabled = false;
        }



        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                mouseDown = true;
            }
            if (Input.GetMouseButton(0))
            {
                mousePress = true;
            }
            if (Input.GetMouseButtonUp(0))
            {
                mouseUp = true;
            }

            if (Input.GetKey(KeyCode.T))
            {
                Application.CaptureScreenshot("Capture_"+DateTime.Now.ToLongTimeString()+".png");
            }
        }

        void FixedUpdate()
        {
           

#region Input Editor win
#if UNITY_EDITOR
            if (mouseDown)
            {
                TouchBall(Input.mousePosition);
                mouseDown = false;
            }


            if (mousePress)
            {
                // if true : that means the user has touched the white ball
                if (bouleBlanche != null)
                    DragBall(new Vector3(Input.mousePosition.x, Input.mousePosition.y, distToBoule)); 

                mousePress = false;
            }

            if (mouseUp)
            {  
                if (bouleBlanche != null)
                    ReleaseBall();
                
                mouseUp = false;
            }

#endif
            #endregion


            #region Input Mobile
#if UNITY_ANDROID

            #region WORK_IN_PROGRESS
            //if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            //{
            //    TouchBall(new Vector3(Input.touches[0].position.x,
            //                          Input.touches[0].position.y, 0));
            //}
            #endregion 
#endif
            #endregion



        }

        public void ResetCoups()
        {
            nbCoups = 0;
            coupTxt.text = "Coups : " + nbCoups;
        }
        
        public void ResetPosition()
        {
            transform.position = spawn.transform.position;
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        }


        public void TouchBall(Vector3 pos)
        {
            RaycastHit hit;
            Ray ray;

            ray = camera.ScreenPointToRay(pos);
            if (Physics.Raycast(ray, out hit, 100))
            {
                if (hit.collider.gameObject.name == gameObject.name)
                {
                    initPosScreen = pos; 
                    bouleBlanche = hit.collider.gameObject;
                    distToBoule = hit.distance;
                    initPos = bouleBlanche.transform.position;
                    //gauge.lo
                }
                else
                    bouleBlanche = null;
            }
        }

        public void DragBall(Vector3 pos)
        {

            gauge.enabled = true;
            gaugeGreen.enabled = true;

            Vector3 cur = camera.ScreenToWorldPoint(new Vector3(pos.x, pos.y, pos.z));
            cur.y = initPos.y;
            dirShoot = (initPos - cur);
            norm = Mathf.Clamp(dirShoot.magnitude, 0f, MAX_POWER);
           

            gaugeGreen.fillAmount = norm / MAX_POWER;

            float angle = Vector3.Angle(Vector3.forward, dirShoot.normalized);
            float aux_angle = Vector3.Angle(Vector3.left, dirShoot.normalized);
            if (aux_angle > 90)
                angle = 360 - angle;

            
            
            gauge.rectTransform.localRotation = Quaternion.AngleAxis(angle, new Vector3(0, 0, 1));
            Vector3 aux_pos = camera.WorldToScreenPoint(transform.position);
            gauge.rectTransform.position = new Vector3(aux_pos.x, aux_pos.y, 0);

        }

        public void ReleaseBall()
        {
            nbCoups++;
            gauge.enabled = false;
            gaugeGreen.enabled = false;
            bouleBlanche.GetComponent<Rigidbody>().AddForce(dirShoot.normalized * dirShoot.magnitude * 300);
            bouleBlanche = null;
            coupTxt.text = "Coups : " + nbCoups;
        }


    }
}