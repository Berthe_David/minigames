﻿using UnityEngine;
using System.Collections;

public class Plateau : MonoBehaviour {

    Rigidbody rb;
    public float x, z;

	void Start () {
        Physics.gravity = new Vector3(0, -98.1f, 0);
        rb = GetComponent<Rigidbody>();
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
            x = 1f;
        if (Input.GetKeyUp(KeyCode.LeftArrow))
            x = 0f;

        if (Input.GetKey(KeyCode.RightArrow))
            x = -1f;
        if (Input.GetKeyUp(KeyCode.RightArrow))
            x = 0f;

        if (Input.GetKey(KeyCode.UpArrow))
            z = -1f;
        if (Input.GetKeyUp(KeyCode.UpArrow))
            z = 0f;

        if (Input.GetKey(KeyCode.DownArrow))
            z = 1f;
        if (Input.GetKeyUp(KeyCode.DownArrow))
            z = 0f;


    }

    void FixedUpdate () {

        rb.AddForce(0, -100, 0);
        Quaternion rot = Quaternion.Euler(x, 0, z);
        rb.MoveRotation(rb.rotation * rot);
    }
}
