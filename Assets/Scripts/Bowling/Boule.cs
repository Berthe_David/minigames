﻿using UnityEngine;
using System.Collections;

public class Boule : MonoBehaviour {

    bool landed;

    public float valeur;
    public float valeurSpin;
    public float velocity;

    public Camera camera;
    private GameObject balle;
    public GameObject viseur;
    private Vector3 initPos;
    private Vector3 initPosViseur;
    private Vector3 initPosPivot;
    private Quaternion initRotaBalle;
    public Vector3 dirBalle;
    public float distToBalle;


    bool bouleHit = false;
    bool viseurHit = false;

    public delegate void Launch();
    public static event Launch OnLaunch;
    bool launched = false;

    void Start () {
        distToBalle = 1;
        Physics.gravity = new Vector3(0, -98.1f, 0);
        valeurSpin = 0;
        landed = false;
        dirBalle.Set(0, 0, 1);
        GetComponent<Rigidbody>().maxDepenetrationVelocity = 10f;
        initRotaBalle = transform.rotation;
        viseur = GameObject.Find("pivot");
    }

    
 


    void FixedUpdate()
    {
        

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100))
            {
                if (hit.collider.gameObject.name == "Balle")
                {

                    bouleHit = true;
                    distToBalle = hit.distance;
                    initPos = transform.position;
                }
                else
                    bouleHit = false;

                if (hit.collider.gameObject.name == "pivot")
                {
                    //viseur = hit.collider.gameObject;
                    viseurHit = true;
                    initPosViseur = Input.mousePosition;
                    initPosPivot = camera.WorldToScreenPoint(transform.position);

                }
                else
                    viseurHit = false;
            }
        }

     

        if (landed)
        {
            dirBalle = Quaternion.Euler(0, valeurSpin, 0) * dirBalle;
            GetComponent<Rigidbody>().AddForce(dirBalle * valeur, ForceMode.Force);
            if (GetComponent<Rigidbody>().velocity.magnitude >= velocity)
            {
                Vector3 v = GetComponent<Rigidbody>().velocity;
                float y = v.y;
                v.y = 0;
                v = v.normalized * velocity;
                GetComponent<Rigidbody>().velocity = new Vector3(v.x, y, v.z);
                
            }
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            landed = true;
            viseur.SetActive(false);
        }

        if (Input.GetMouseButton(0) && !landed)
        {
            if (bouleHit)
            {
                Vector3 vv = camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, distToBalle));
                vv.y = initPos.y;
                vv.z = initPos.z;
                transform.position = vv;
            }

            if (viseurHit)
            {
                Vector3 dirInit = (initPosViseur - initPosPivot).normalized;
                Vector3 dirCur = (Input.mousePosition - initPosPivot).normalized;

                float ang = Vector3.Angle(dirInit, dirCur);
                Debug.Log("ang : " + ang);
                if (Input.mousePosition.x < initPosViseur.x)
                {

                    transform.rotation = initRotaBalle * Quaternion.Euler(0, -ang, 0);
                    valeurSpin = (ang) / 80.0f;
                    dirBalle = Quaternion.Euler(0, -ang, 0) * Vector3.forward;
                }
                else
                {
                    transform.rotation = initRotaBalle * Quaternion.Euler(0, ang, 0);
                    valeurSpin = (-ang) / 80.0f;
                    dirBalle = Quaternion.Euler(0, ang, 0) * Vector3.forward;
                }
            }
                
        }



        if (Input.GetKey(KeyCode.B))
        {
            dirBalle = Quaternion.Euler(0, -0.1f, 0) * dirBalle;
            GameObject.Find("Balle").transform.rotation *= Quaternion.Euler(0, -0.1f, 0);
        }

        if (Input.GetKey(KeyCode.N))
        {
            dirBalle = Quaternion.Euler(0, 0.1f, 0) * dirBalle;
            GameObject.Find("Balle").transform.rotation *= Quaternion.Euler(0, 0.1f, 0);
        }
        if (Input.GetKey(KeyCode.T))
        {
            Application.CaptureScreenshot("Capture.png");
        }
        //valeurSpin += Input.GetAxis("Horizontal") * Time.deltaTime / 3f;
        //Mathf.Clamp(valeurSpin, -1, 1);


    }


    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "checkerQuilles")
        {
            OnLaunch();
        }
    }

    public void Reset()
    {
        valeurSpin = 0;
        landed = false;
        dirBalle.Set(0, 0, 1);
        transform.rotation = initRotaBalle;
        GetComponent<Rigidbody>().Sleep();
        viseur.SetActive(true);

    }
}
