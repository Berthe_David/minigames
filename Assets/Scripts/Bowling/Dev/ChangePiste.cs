﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChangePiste : MonoBehaviour {

    GameObject[] pistes;
    public int active = 0;
    public int prev;
    GameObject actuel;
    GameObject next;
    Vector3 initPosActual;
    Vector3 initPosNext;
    Animator anim;
    bool canChange = true;

    void Start () {
        pistes = GameObject.FindGameObjectsWithTag("Piste");
        actuel = GameObject.Find("DummyActive");
        initPosActual = actuel.transform.position;
        next = GameObject.Find("DummyNext");
        initPosNext = next.transform.position;

        //pistes[0].transform.SetParent(actuel.transform);
        pistes[0].transform.position = initPosActual;
        pistes[0].transform.parent = actuel.transform;
        //actuel.transform.position = initPosActual;
        anim = GetComponent<Animator>();
    }
	
    public void Change(int i)
    {
        if (canChange)
        {
            canChange = false;
            prev = active;
            active += i;
            if (active >= pistes.Length)
                active = 0;
            if (active < 0)
                active = pistes.Length - 1;

            pistes[active].transform.position = initPosNext;
            pistes[active].transform.parent = next.transform;

            //play anim
            anim.Play("EchangePiste");
        }
    }
	
    public void UpdateDummy()
    {
        pistes[prev].transform.parent = null;
        pistes[active].transform.parent = null;

        actuel.transform.position = initPosActual;
        next.transform.position = initPosNext;

        pistes[active].transform.position = initPosActual;

        pistes[active].transform.parent = actuel.transform;
        canChange = true;
    }

}
