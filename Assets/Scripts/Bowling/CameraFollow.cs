﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    public Transform m_target;
    public float m_smoothing = 4f;
    public float distance = 10;

    private Vector3 offset;
    private Vector3 initPos;
	void Awake () {
        offset = transform.position - m_target.position;
        initPos = transform.position;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        Vector3 targetCamPos = m_target.position + offset;
        targetCamPos.x = initPos.x;
        if(Vector3.Magnitude(targetCamPos - initPos) < distance)
            transform.position = Vector3.Lerp(transform.position, targetCamPos, m_smoothing * Time.deltaTime);
	}
}
