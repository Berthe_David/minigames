﻿using UnityEngine;
using System.Collections;

public class Quille : MonoBehaviour {

    Rigidbody rb;
    bool canCheck;
    bool didCheck;
    float angle;
    
	void Awake () {
        rb = GetComponent<Rigidbody>();
        canCheck = didCheck = false;
        
	}

    void Start()
    {
        Boule.OnLaunch += launch;
        StartCoroutine(check());
    }

    void launch()
    {
        //StartCoroutine(check());
        canCheck = true;
    }

    IEnumerator check()
    {
        yield return new WaitForSeconds(3.0f);
        Debug.Log("C'est Parti !");
        for (;;)
        {
            Debug.Log("CanCheck ?");
            if (canCheck)
            {
                Debug.Log("CanCheck !");
                if (!didCheck && rb.velocity.magnitude <= 1e-3)
                {
                    Debug.Log("Check !");
                    angle = Vector3.Angle(transform.up, Vector3.up);
                    if (Vector3.Angle(transform.up, Vector3.up) > 15)
                        InfoGame.Up();
                    InfoGame.Done();
                    didCheck = true;
                    canCheck = false;
                    //break;
                }
            }
            yield return new WaitForSeconds(1.0f);

        }
    }

    public void Reset()
    {
        didCheck = false;
    }

    
}
