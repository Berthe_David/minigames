﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class InfoGame : MonoBehaviour {


    public int _nbQuilles;
    public int _cptAll = 0;
    public static int s_cptFall = 0;
    public static int s_cptDone = 0;
    public int _idPiste = 0;

    //List<GameObject> _lquilles;
    public GameObject[] _quilles;
    //List<GameObject> _lspawns;
    public GameObject[] _spawns;
    public List<GameObject> _spawnsBalle;
    public List<GameObject> _spawnsQuille;
    public GameObject _spawnBalle;
    public GameObject _spawnQuille;

    GameObject _balle;
    // offset : distance de chaque quille au point de spawn 
    // et celle de la balle
    List<Vector3> _offsetQuilles;
    public List<Quaternion> _rotationQuilles;
    Quaternion _rotationQuille;
    Vector3 _offsetBalle;

    Text score;

    void Awake() {
        _quilles = GameObject.FindGameObjectsWithTag("Quille");
        _nbQuilles = _quilles.Length;
        _spawns = GameObject.FindGameObjectsWithTag("Spawn");
        _balle = GameObject.Find("Balle");

        score = GameObject.Find("Canvas/Score").GetComponent<Text>();

        #region un spawn

        foreach (GameObject s in _spawns)
        {
            int type = (int)s.GetComponent<Spawns>()._type;
            int id = (int)s.GetComponent<Spawns>()._id;

            if (type == (int)TYPE.BALLE)
                _spawnBalle = s;
            else if (type == (int)TYPE.QUILLE)
                _spawnQuille = s;
        }
        _balle.transform.position = _spawnBalle.transform.position;
        #endregion

        #region plusieurs spawns
        // ça n'alloue pas de la place mémoire en déf la cap de la lst
        //_spawnsBalle.Capacity = _spawns.Length / 2;
        //_spawnsQuille.Capacity = _spawns.Length / 2;

        //for (int i = 0; i < _spawnsBalle.Capacity; ++i)
        //    _spawnsBalle.Add(gameObject);
        //for (int i = 0; i < _spawnsQuille.Capacity; ++i)
        //    _spawnsQuille.Add(gameObject);
        //foreach (GameObject go in _quilles) _lquilles.Add(go);
        //foreach (GameObject go in _spawns) _lspawns.Add(go);


        //foreach (GameObject s in _spawns)
        //{
        //    int type = (int)s.GetComponent<Spawns>()._type;
        //    int id = (int)s.GetComponent<Spawns>()._id;


        //    if (type == (int)TYPE.BALLE)
        //        _spawnsBalle[id] = s;
        //    else if (type == (int)TYPE.QUILLE)
        //        _spawnsQuille[id] = s;
        //}

        #endregion pragma


        // Pas necessaire, une seule suffit
        //foreach (GameObject go in _quilles)
        //_rotationQuilles.Add(go.transform.rotation);
        _rotationQuille = _quilles[0].transform.rotation;


        // on stocke les positions des quilles et de la balle par rapport aux
        // spawn points => pour les replacer facilement quand on changer de piste
        _offsetQuilles = new List<Vector3>();
        foreach (GameObject q in _quilles)
        {
            _offsetQuilles.Add(q.transform.position - _spawnQuille.transform.position);
        }
        //_offsetBalle = _spawnBalle.transform.position - _balle.transform.position;

        StartCoroutine(print());
    }
	
    public static void Up()
    {
        s_cptFall++;
    }

    public static void Done()
    {
        s_cptDone++;
    }

    IEnumerator print()
    {
        for (;;)
        { 
            Debug.Log("score : " + s_cptFall+" -- "+s_cptDone+" -- "+_nbQuilles);
            yield return new WaitForSeconds(1.0f);
            if(s_cptDone == _nbQuilles)
            {
                _cptAll += s_cptFall;
                s_cptFall = s_cptDone = 0;
                reset();
                Debug.Log("On recommence !");
            }

            score.text = s_cptFall+" / "+_nbQuilles;
        }
    }

    // Une seule piste pour l'instant
    public void reset(/*int idPiste*/)
    {
        int i = 0;
        foreach(GameObject q in _quilles)
        {
            q.GetComponent<Quille>().Reset();
            q.transform.position = _spawnQuille.transform.position + _offsetQuilles[i];
            q.transform.rotation = _rotationQuille;
            i++;
        }
        //pensez à remmetre la force à zéro
        _balle.GetComponent<Rigidbody>().velocity.Set(0, 0, 0);
        _balle.GetComponent<Boule>().Reset();
        _balle.transform.position = _spawnBalle.transform.position;
    }
}
